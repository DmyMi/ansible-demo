# Ansible example

Simple Ansible playbook with some common things to show. Works on Ubuntu :)

## How to run

1. Install required collections: `ansible-galaxy collection install -r requirements.yml -p ./collections`
2. Install optional roles: `ansible-galaxy role install -r requirements.yml -p ./roles_external`. Need to uncomment code in `site.yaml` to run
3. Edit the inventory to have your server/VM IP
4. Run with `ansible-playbook site.yml --limit example -K`
